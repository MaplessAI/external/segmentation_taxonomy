#ifndef SEGMENTATION_TAXONOMY__VISIBILITY_CONTROL_H_
#define SEGMENTATION_TAXONOMY__VISIBILITY_CONTROL_H_

// This logic was borrowed (then namespaced) from the examples on the gcc wiki:
//     https://gcc.gnu.org/wiki/Visibility

#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define SEGMENTATION_TAXONOMY_EXPORT __attribute__ ((dllexport))
    #define SEGMENTATION_TAXONOMY_IMPORT __attribute__ ((dllimport))
  #else
    #define SEGMENTATION_TAXONOMY_EXPORT __declspec(dllexport)
    #define SEGMENTATION_TAXONOMY_IMPORT __declspec(dllimport)
  #endif
  #ifdef SEGMENTATION_TAXONOMY_BUILDING_LIBRARY
    #define SEGMENTATION_TAXONOMY_PUBLIC SEGMENTATION_TAXONOMY_EXPORT
  #else
    #define SEGMENTATION_TAXONOMY_PUBLIC SEGMENTATION_TAXONOMY_IMPORT
  #endif
  #define SEGMENTATION_TAXONOMY_PUBLIC_TYPE SEGMENTATION_TAXONOMY_PUBLIC
  #define SEGMENTATION_TAXONOMY_LOCAL
#else
  #define SEGMENTATION_TAXONOMY_EXPORT __attribute__ ((visibility("default")))
  #define SEGMENTATION_TAXONOMY_IMPORT
  #if __GNUC__ >= 4
    #define SEGMENTATION_TAXONOMY_PUBLIC __attribute__ ((visibility("default")))
    #define SEGMENTATION_TAXONOMY_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define SEGMENTATION_TAXONOMY_PUBLIC
    #define SEGMENTATION_TAXONOMY_LOCAL
  #endif
  #define SEGMENTATION_TAXONOMY_PUBLIC_TYPE
#endif

#endif  // SEGMENTATION_TAXONOMY__VISIBILITY_CONTROL_H_
